    <footer>
        Site développé par HBFC <br>
        Rayan-Ali Baghazou, Maxime Rochette, Coline Gauchez, Boris Baskovec, Hugo Verdun, Valentin Seon, Vincent Sagnard

        <div>
            Icônes par
            <a href="http://www.flaticon.com/authors/roundicons" title="Roundicons">
                Roundicons
            </a> sur
            <a href="http://www.flaticon.com" title="Flaticon">
                www.flaticon.com</a> sous license
            <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">
                CC 3.0 BY
            </a>
        </div>
    </footer>

    <script src="vendor/jquery/jquery-3.1.1.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/base.js"></script>

    </body>
</html>