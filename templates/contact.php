<?php require_once 'nav.php' ?>

<div class="container-fluid louer-contact">
    <div class="row">
        <div class="container centre-vertical">
            <div class="col-xs-12">
                <div class="titre">
                    <h1>Contact</h1>
                </div>
                <form method="post">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="sujet">Sujet</label>
                            <select name="sujet" id="sujet" class="form-control">
                                <option value="1">Problème technique</option>
                                <option value="2">Question par rapport à une location</option>
                                <option value="3">Autre</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="text-center">
                            <input class="btn btn-success" type="submit" value="Envoyer"/>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>