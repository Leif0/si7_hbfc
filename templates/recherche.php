<?php require_once 'nav.php' ?>

<div class="container-fluid louer-recherche">
    <div class="row">
        <div class="container">
            <div class="col-xs-12">
                <h1>Location de véhicule</h1>

                <?php foreach ($vehicules as $vehicule) : ?>
                    <div class="vehicule">
                        <h2><?php echo $vehicule->getModele(); ?></h2>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Mise en circulation</th>
                                <th>Couleur</th>
                                <th>Portes</th>
                                <th>Puissance fiscale</th>
                                <th>Boite de vitesse</th>
                                <th>Constructeur</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td><?php echo $vehicule->getMiseEnCirculation(); ?></td>
                                <td><?php echo $vehicule->getCouleurExterieure(); ?></td>
                                <td><?php echo $vehicule->getNombrePortes(); ?></td>
                                <td><?php echo $vehicule->getPuissanceFiscale(); ?></td>
                                <td><?php echo $vehicule->getBoiteDeVitesse(); ?></td>
                                <td><?php echo $vehicule->getConstructeur()->getNom(); ?></td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-xs-2 col-xs-offset-10 text-center reserver">
                                <div class="btn btn-primary">
                                    Réserver
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>