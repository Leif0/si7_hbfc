<?php require_once 'nav.php' ?>

<div class="container-fluid louer-location">
    <div class="row">
        <div class="container centre-vertical">
            <div class="col-xs-12 col-sm-5">

                <h1>Location de véhicule</h1>

                <div class="description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facultas posidonium patientiamque eas. Minimum. Sermo inquit, mundus quosvis in commemorandis provincias amarissimam possunt error ortum. Putem ipsas tantam ipsam principio mollitia.
                </div>

            </div>

            <div class="col-xs-12 col-sm-7 formulaire">
                <div class="titre">
                    <h2>Lorem ipsum</h2>
                </div>
                <form action="index.php?page=recherche" method="post">

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="debut">Date de début</label>
                            <input type="date" class="form-control" name="debut" id="debut"/>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fin">Date de fin</label>
                            <input type="date" class="form-control" name="fin" id="fin"/>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="type-vehicule-selecteur">
                                <input checked="checked" id="citadine" type="radio" name="type_vehicule" value="citadine"/>
                                <label class="label-type-vehicule citadine" for="citadine">Citadine</label>
                                <input id="utilitaire" type="radio" name="type_vehicule" value="utilitaire" />
                                <label class="label-type-vehicule utilitaire" for="utilitaire">Utilitaire</label>
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="text-center">
                            <input class="btn btn-success" type="submit" value="Rechercher"/>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>