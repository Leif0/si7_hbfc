<?php require_once 'nav.php' ?>

<div class="container-fluid louer-connexion">
    <div class="row">
        <div class="container centre-vertical">
            <div class="col-xs-12 col-sm-5 texte-accueil">

                <h1>Location de véhicule</h1>

                <div class="description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facultas posidonium patientiamque eas. Minimum. Sermo inquit, mundus quosvis in commemorandis provincias amarissimam possunt error ortum. Putem ipsas tantam ipsam principio mollitia.
                </div>

            </div>

            <div class="col-xs-12 col-sm-7 formulaire">
                <div class="titre">
                    <h2>Connexion</h2>
                </div>

                <?php afficher_erreurs(); ?>

                <form action="index.php?page=connexion" method="post">

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="mail">Mail</label>
                            <input type="email" name="mail" id="mail" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" name="password" id="password" class="form-control"/>
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="text-center">
                            <input class="btn btn-success" type="submit" value="Connexion"/>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>