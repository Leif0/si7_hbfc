<div class="row menu-administration">
    <div class="col-xs-12 col-sm-4 type-option">
        <div class="contenu">
            <a href="index.php?page=administration&type=utilisateurs">
                Utilisateurs
            </a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4 type-option">
        <div class="contenu">
            <a href="index.php?page=administration&type=vehicules">
                Véhicules
            </a>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4 type-option">
        <div class="contenu">
            <a href="index.php?page=administration&type=prets">
                Prêts
            </a>
        </div>
    </div>
</div>