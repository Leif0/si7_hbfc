<?php

$type = $_GET['type'];

switch($type){

    /**
     * Page d'administration des utilisateurs
     * Affiche un tableau de tous les utilisateurs
     * Possibilité d'ajouter, supprimer ou de modifier un utilisateur
     */

    case 'utilisateurs':
        $utilisateurs = $BDD->CreerUtilisateurs();
        $roles = $BDD->CreerRoles();
        require_once realpath(__DIR__ . '/../utilisateurs/formulaire.php');
        require_once realpath(__DIR__ . '/../utilisateurs/tableau.php');
        break;

    /**
     * Page d'administration des véhicules
     * Affiche un tableau de tous les véhicules
     * Possibilité d'ajouter, supprimer ou de modifier un véhicules
     */

    case 'vehicules':
        $constructeurs = $BDD->CreerConstructeurs();
        $vehicules = $BDD->CreerVehicules();
        require_once realpath(__DIR__ . '/../vehicules/formulaire.php');
        require_once realpath(__DIR__ . '/../vehicules/tableau.php');
        break;
}

?>
