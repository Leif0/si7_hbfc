<?php require_once 'nav.php' ?>

<div class="container-fluid louer-administration">
    <div class="row">
        <div class="container page">
            <div class="col-xs-12 text-center">
                <h1>Administration</h1>

                <?php

                require_once 'menu.php';

                if(isset($_GET['type'])){
                    require_once 'gestion.php';
                }

                ?>
                
            </div>
        </div>
    </div>
</div>