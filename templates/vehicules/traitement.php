<?php

if(isset($_POST['type_traitement']) && $_POST['type_traitement'] == 'vehicule'){

    $champs = ['immatriculation', 'modele', 'mise_en_circulation', 'couleur_exterieure',
        'nombre_portes', 'puissance_fiscale', 'boite_de_vitesse', 'id_constructeur'];

    $erreurs = [];

    foreach ($champs as $champ){
        if(isset($_POST[$champ]) && $_POST[$champ] == ''){
            array_push($erreurs, 'Merci de renseigner ce champ : ' . $champ);
        }
    }

    $_SESSION['erreurs'] = $erreurs;

    // Si il n'y a pas d'erreurs

    if(count($erreurs) == 0){

        try{
            if(isset($_POST['id']) && $_POST['id'] != null){

                // On modifie le véhicule

                $resultat = $BDD->modifierVehicule(
                    $_POST['id'],
                    $_POST['immatriculation'],
                    $_POST['modele'],
                    $_POST['mise_en_circulation'],
                    $_POST['couleur_exterieure'],
                    $_POST['nombre_portes'],
                    $_POST['puissance_fiscale'],
                    $_POST['boite_de_vitesse'],
                    $_POST['id_constructeur']
                );

            }else{

                // On ajoute le véhicule

                $resultat = $BDD->InsererVehicule(
                    $_POST['immatriculation'],
                    $_POST['modele'],
                    $_POST['mise_en_circulation'],
                    $_POST['couleur_exterieure'],
                    $_POST['nombre_portes'],
                    $_POST['puissance_fiscale'],
                    $_POST['boite_de_vitesse'],
                    $_POST['id_constructeur']
                );

                echo $resultat;
            }

            // On recharge les utilisateurs pour éviter de devoir recharger la page
            $utilisateurs = $BDD->CreerVehicules();

        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}