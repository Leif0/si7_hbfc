<?php require_once 'traitement.php' ?>

<div id="bouton-ajouter-vehicule" class="row">
    <div class="col-xs-12">
        <div class="text-center">
            <div class="btn btn-default btn-lg">
                Ajouter un véhicule
            </div>
        </div>
    </div>
</div>

<?php afficher_erreurs(); ?>

<div id="formulaire-vehicule" class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3 formulaire">
        <form method="POST">

            <!-- Un ID véhicule, si il n'est pas vide c'est qu'on modifie un véhicule -->
            <input type="hidden" name="id" id="id" value="" />
            <input type="hidden" name="type_traitement" id="type_traitement" value="vehicule" />

            <!-- Immatriculation -->

            <div class="form-group">
                <label for="immatriculation">Immatriculation</label>
                <input type="text" class="form-control" name="immatriculation" id="immatriculation" value="AC-578-GE">
            </div>

            <!-- Modèle -->

            <div class="form-group">
                <label for="modele">Modèle</label>
                <input type="text" class="form-control" name="modele" id="modele" value="Primera">
            </div>

            <!-- Mise en circulation -->

            <div class="form-group">
                <label for="mise_en_circulation">Mise en circulation</label>
                <input type="date" class="form-control" name="mise_en_circulation" id="mise_en_circulation" value="2005-01-01">
            </div>

            <!-- Couleur extérieur -->

            <div class="form-group">
                <label for="couleur_exterieure">Couleur extérieur</label>
                <input type="text" class="form-control" name="couleur_exterieure" id="couleur_exterieure" value="Orange">
            </div>

            <!-- Nombre de portes -->

            <div class="form-group">
                <label for="nombre_portes">Nombre de portes</label>
                <input type="text" class="form-control" name="nombre_portes" id="nombre_portes" value="5">
            </div>

            <!-- Puissance fiscale -->

            <div class="form-group">
                <label for="puissance_fiscale">Puissance fiscale</label>
                <input type="text" class="form-control" name="puissance_fiscale" id="puissance_fiscale" value="5">
            </div>

            <!-- Boite de vitesse -->

            <div class="form-group">
                <label for="boite_de_vitesse">Boite le vitesse</label>
                <select name="boite_de_vitesse" id="boite_de_vitesse" class="form-control">
                    <option value="0">Manuel</option>
                    <option value="1">Automatique</option>
                </select>
            </div>

            <!-- Constructeur -->

            <div class="form-group">
                <label for="id_constructeur">Constructeur</label>
                <select name="id_constructeur" id="id_constructeur" class="form-control">
                    <?php foreach ($constructeurs as $constructeur) : ?>
                        <option value="<?= $constructeur->getId() ?>"><?= $constructeur->getNom() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Envoyer</button>
        </form>
    </div>
</div>
