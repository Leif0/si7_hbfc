<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Immatriculation</th>
        <th>Modèle</th>
        <th>Mise en circulation</th>
        <th>Couleur extérieure</th>
        <th>Nombre de portes</th>
        <th>Puissance fiscale</th>
        <th>Boite de vitesse</th>
        <th>Constructeur</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($vehicules as $vehicule): ?>
    <tr id="ligne-<?= $vehicule->getId() ?>">
        <th class="id"><?= $vehicule->getId() ?></th>
        <td class="immatriculation"><?= $vehicule->getImmatriculation() ?></td>
        <td class="modele"><?= $vehicule->getModele() ?></td>
        <td class="mise_en_circulation"><?= $vehicule->getMiseEnCirculation() ?></td>
        <td class="couleur_exterieure"><?= $vehicule->getCouleurExterieure() ?></td>
        <td class="nombre_de_portes"><?= $vehicule->getNombrePortes() ?></td>
        <td class="puissance_fiscale"><?= $vehicule->getPuissanceFiscale() ?></td>
        <td class="boite_de_vitesse"><?= $vehicule->getBoiteDeVitesse() ?></td>
        <td class="constructeur"><?= $vehicule->getConstructeur()->getNom() ?></td>

        <td>
            <div data-id="<?= $vehicule->getId() ?>" class="modifier btn btn-sm btn-warning">Modifier</div>
            <div data-id="<?= $vehicule->getId() ?>" class="supprimer btn btn-sm btn-danger">Supprimer</div>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>