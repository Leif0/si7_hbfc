<?php

if(isset($_POST['type_traitement']) && $_POST['type_traitement'] == 'utilisateur'){

    $champs = ['nom', 'prenom', 'telephone', 'mail', 'id_role'];

    $erreurs = [];

    foreach ($champs as $champ){
        if(isset($_POST[$champ]) && $_POST[$champ] == ''){
            array_push($erreurs, 'Merci de renseigner ce champ : ' . $champ);
        }
    }

    $_SESSION['erreurs'] = $erreurs;

    // Si il n'y a pas d'erreurs

    if(count($erreurs) == 0){

        try{
            if(isset($_POST['id']) && $_POST['id'] != null){

                // On modifie l'utilisateur

                $resultat = $BDD->modifierUtilisateur(
                    $_POST['id'],
                    $_POST['nom'],
                    $_POST['prenom'],
                    $_POST['telephone'],
                    $_POST['mail'],
                    $_POST['id_role']
                );

            }else{

                // On ajoute l'utilisateur

                $resultat = $BDD->insererUtilisateur(
                    $_POST['nom'],
                    $_POST['prenom'],
                    $_POST['telephone'],
                    $_POST['mail'],
                    $_POST['id_role']
                );
            }

            // mysql renvoie 23000 si le champs unique n'est pas unique
            if($resultat == '23000'){
                echo "Adresse email déjà utilisée";
            }

            // On recharge les utilisateurs pour éviter de devoir recharger la page
            $utilisateurs = $BDD->CreerUtilisateurs();

        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}