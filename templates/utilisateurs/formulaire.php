<?php require_once 'traitement.php' ?>

<div id="bouton-ajouter-utilisateur" class="row">
    <div class="col-xs-12">
        <div class="text-center">
            <div class="btn btn-default btn-lg">
                Ajouter un utilisateur
            </div>
        </div>
    </div>
</div>

<?php afficher_erreurs(); ?>

<div id="formulaire-utilisateur" class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3 formulaire">
        <form method="POST">

            <!-- Un ID utilisateur, si il n'est pas vide c'est qu'on modifie un utilisateur -->
            <input type="hidden" name="id" id="id" value="" />
            <input type="hidden" name="type_traitement" id="type_traitement" value="utilisateur" />

            <!-- Nom -->

            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" name="nom" id="nom">
            </div>

            <!-- Prénom -->

            <div class="form-group">
                <label for="prenom">Prénom</label>
                <input type="text" class="form-control" name="prenom" id="prenom">
            </div>

            <!-- Téléphone -->

            <div class="form-group">
                <label for="telephone">Téléphone</label>
                <input type="text" class="form-control" name="telephone" id="telephone">
            </div>

            <!-- Mail -->

            <div class="form-group">
                <label for="mail">Adresse email</label>
                <input type="text" class="form-control" name="mail" id="mail">
            </div>

            <!-- Mot de passe -->

            <!--<div class="form-group">
                <label for="mot_de_passe">Mot de passe</label>
                <input type="text" class="form-control" name="mot_de_passe" id="mot_de_passe">
            </div>-->

            <!-- Role -->

            <div class="form-group">
                <label for="id_role">Rôle</label>
                <select name="id_role" id="id_role" class="form-control">
                    <?php foreach ($roles as $role) : ?>
                        <option value="<?= $role->getId() ?>"><?= $role->getLibelle() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <!-- Bouton d'envoie -->

            <button type="submit" class="btn btn-success">Envoyer</button>
        </form>
    </div>
</div>
