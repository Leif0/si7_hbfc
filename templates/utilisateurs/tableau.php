<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Téléphone</th>
        <th>Mail</th>
        <th>Rôle</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($utilisateurs as $utilisateur): ?>
    <tr id="ligne-<?= $utilisateur->getId() ?>">
        <th class="id"><?= $utilisateur->getId() ?></th>
        <td class="nom"><?= $utilisateur->getNom() ?></td>
        <td class="prenom"><?= $utilisateur->getPrenom() ?></td>
        <td class="telephone"><?= $utilisateur->getTelephone() ?></td>
        <td class="mail"><?= $utilisateur->getMail() ?></td>
        <td class="role" data-id="<?= $utilisateur->getRole()->getId() ?>">
            <?= $utilisateur->getRole()->getLibelle() ?>
        </td>
        <td>
            <div data-id="<?= $utilisateur->getId() ?>" class="modifier-utilisateur btn btn-sm btn-warning">Modifier</div>
            <div data-id="<?= $utilisateur->getId() ?>" class="supprimer-utilisateur btn btn-sm btn-danger">Supprimer</div>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>