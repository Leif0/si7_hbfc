<?php

/*
 *  Supprime un utilisateur de la base de donnée
 */

session_start();
if(isset($_SESSION['role']) && $_SESSION['role'] == 1 && $_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['id']) && $_POST['id'] != null){

        // On doit require la BDD et la créer car c'est un fichier idnépendant

        require_once '../../class/BDD.php';
        $BDD = new BDD('localhost', 'location_voiture', 'utf8', 'root', '');

        $idUtilisateur = intval($_POST['id']);

        $resultat = $BDD->SupprimerUtilisateurParID($idUtilisateur);
        header('Content-Type: application/json');

        $resultat = [
            'result' => $resultat,
            'idUtilisateur' => $idUtilisateur
        ];

        echo json_encode($resultat);
    }
}