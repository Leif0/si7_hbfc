<?php


/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 10/11/2016
 * Time: 14:02
 */
class BDD
{
    private $host = 'localhost';
    private $nom_base = 'location_voiture';
    private $charset = 'utf8';
    private $utilisateur = 'root';
    private $password = '';
    private $acces;

    /**
     * BDD constructor.
     * @param $host
     * @param $nom_base
     * @param $charset
     * @param $utilisateur
     * @param $password
     */
    public function __construct()
    {
        // Si on est en prod, on met le mot de passe
        if($_SERVER['SERVER_NAME'] == '192.168.10.202'){
            $this->password = 'admadm42';
        }
        
        try
        {
            $this->acces = new PDO(
                'mysql:host=' . $this->host . ';dbname=' . $this->nom_base . ';charset=' . $this->charset,
                $this->utilisateur,
                $this->password
            );

            // Afficher les erreurs en dev

            $this->acces->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->acces->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }
    }

    /******************
     ** UTILISATEURS **
     ******************/

    /**
     * Créer tous les utilisateurs
     * @return array
     */
    public function CreerUtilisateurs(){

        // On a besoin de la classe Utilisateur
        require_once 'Utilisateur.php';

        // Notre requete SQL
        $requete = $this->acces->query('SELECT * FROM utilisateur');

        // Tableau vide pour les utilisateurs
        $utilisateurs = [];

        // Créer tous les utilisateurs
        while($utilisateur = $requete->fetch(PDO::FETCH_OBJ)){
            $role = $this->CreerRoleParID($utilisateur->id_role);

            $utilisateur = new Utilisateur(
                $utilisateur->id,
                $utilisateur->nom,
                $utilisateur->prenom,
                $utilisateur->telephone,
                $utilisateur->mail,
                $role
            );

            array_push($utilisateurs, $utilisateur);
        }

        return $utilisateurs;
    }

    public function SupprimerUtilisateurParID($id){

        // On a besoin de la classe Utilisateur
        require_once 'Utilisateur.php';

        // Notre requete SQL
        $requete = $this->acces->prepare('DELETE FROM utilisateur WHERE id = :id');

        $resultat = $requete->execute([
            'id' => $id
        ]);

        return $resultat;
    }

    public function ConnexionUtilisateur($mail, $password){
        // Notre requete SQL
        $requete = $this->acces->prepare('SELECT id, mail, password FROM utilisateur WHERE mail = :mail');

        $resultat = $requete->execute([
            'mail' => $mail
        ]);

        // On a trouvé un utilisateur avec cette adresse email
        if($resultat){
            $ligne = $requete->fetch();
            $password_ok = password_verify($password, $ligne['password']);

            // Le mot de passe est bon
            if($password_ok){
                return $ligne['id'];
            }
        }

        return 0;
    }

    public function CreerRoles()
    {
        // On a besoin de la classe Role
        require_once 'Role.php';

        // Notre requete SQL
        $requete = $this->acces->query('SELECT * FROM role');

        // Tableau vide pour les roles
        $roles = [];

        // Créer tous les utilisateurs
        while($role = $requete->fetch(PDO::FETCH_OBJ)){
            $role = new Role(
                $role->id,
                $role->libelle
            );

            array_push($roles, $role);
        }

        return $roles;
    }

    public function CreerRoleParID($id)
    {
        // On a besoin de la classe Role
        require_once 'Role.php';

        // Notre requete SQL
        $requete = $this->acces->prepare('SELECT * FROM role WHERE id = :id');

        $resultat = $requete->execute([
            'id' => $id
        ]);

        if($resultat){
            $role = $requete->fetch(PDO::FETCH_OBJ);

            $role = new Role(
                $role->id,
                $role->libelle
            );

            return $role;
        }
    }

    public function InsererUtilisateur($nom, $prenom, $telephone, $mail, $id_role){
        try{
            $requete = $this->acces->prepare(
                'INSERT INTO utilisateur VALUES(DEFAULT, :nom, :prenom, :telephone, :mail, :mot_de_passe, :id_role)'
            );

            $mot_de_passe = "123456";

            $resultat = $requete->execute([
                'nom' => $nom,
                'prenom' => $prenom,
                'telephone' => $telephone,
                'mail' => $mail,
                'mot_de_passe' => password_hash($mot_de_passe, PASSWORD_DEFAULT),
                'id_role' => $id_role
            ]);

            if($resultat == 1){
                echo 'Utilisateur ajouté ! ';
            }
        }catch(Exception $e){

            // Adresse email déjà utilisée, on renvoie le code

            if($e->getCode() == '23000'){
                return $e->getCode();
            }
        }
    }

    public function modifierUtilisateur($id, $nom, $prenom, $telephone, $mail, $id_role){
        try{

            $requete = $this->acces->prepare(
                "UPDATE utilisateur SET nom = :nom, prenom = :prenom, telephone = :telephone, 
                  mail = :mail, id_role = :id_role WHERE id = :id");

            $resultat = $requete->execute([
                'nom' => $nom,
                'prenom' => $prenom,
                'telephone' => $telephone,
                'mail' => $mail,
                'id_role' => intval($id_role),
                'id' => intval($id)
            ]);

            if($resultat == 1){
                echo 'Utilisateur modifié';
            }else{
                echo 'Erreur lors de la modification';
            }

        }catch(Exception $e){
            var_dump($e);
        }
    }

    /*******************
     **   VEHICULES   **
     ******************/

    public function CreerVehicules()
    {
        // On a besoin de la classe Vehicule
        require_once 'Vehicule.php';

        // Notre requete SQL
        $requete = $this->acces->query('SELECT * FROM vehicule');

        // Tableau vide pour les vehicules
        $vehicules = [];

        // Créer tous les vehicules
        while($vehicule = $requete->fetch(PDO::FETCH_OBJ)){

            $constructeur = $this->CreerConstructeurParID($vehicule->id_constructeur);

            $vehicule = new Vehicule(
                $vehicule->id,
                $vehicule->immatriculation,
                $vehicule->modele,
                $vehicule->mise_en_circulation,
                $vehicule->couleur_exterieure,
                $vehicule->nombre_portes,
                $vehicule->puissance_fiscale,
                $vehicule->boite_de_vitesse,
                $constructeur
            );

            array_push($vehicules, $vehicule);
        }

        return $vehicules;
    }

    public function InsererVehicule($immatriculation, $modele, $mise_en_circulation, $couleur_exterieure,
                                    $nombre_portes, $puissance_fiscale, $boite_de_vitesse, $id_constructeur){
        try{

            $requete = $this->acces->prepare(
                'INSERT INTO vehicule 
                 VALUES(DEFAULT, :immatriculation, :modele, :mise_en_circulation, :couleur_exterieure, 
                                 :nombre_portes, :puissance_fiscale, :boite_de_vitesse, :id_constructeur)'
            );

            $resultat = $requete->execute([
                ':immatriculation' => $immatriculation,
                ':modele' => $modele,
                ':mise_en_circulation' => $mise_en_circulation,
                ':couleur_exterieure' => $couleur_exterieure,
                ':nombre_portes' => $nombre_portes,
                ':puissance_fiscale' => $puissance_fiscale,
                ':boite_de_vitesse' => $boite_de_vitesse,
                ':id_constructeur' => $id_constructeur
            ]);

            if($resultat == 1){
                echo 'Véhicule ajouté ! ';
            }else{
                echo 'Erreur : Véhicule non ajouté';
            }

            return $resultat;
        }catch(Exception $e){
            if($e->getCode() == '23000'){
                echo 'Erreur 23000 : Immatriculation utilisée.';
            }else{
                die(var_dump($e));
            }
        }
    }

    public function SupprimerVehiculeParID($id){

        // On a besoin de la classe Vehicule
        require_once 'Vehicule.php';

        // Notre requete SQL
        $requete = $this->acces->prepare('DELETE FROM vehicule WHERE id = :id');

        $resultat = $requete->execute([
            'id' => $id
        ]);

        return $resultat;
    }


    /***********************
     **   CONSTRUCTEURS   **
     **********************/
    public function CreerConstructeurs()
    {
        // On a besoin de la classe Constructeur
        require_once 'Constructeur.php';

        // Notre requete SQL
        $requete = $this->acces->query('SELECT * FROM constructeur');

        // Tableau vide pour les constructeurs
        $constructeurs = [];

        // Créer tous les vehicules
        while($constructeur = $requete->fetch(PDO::FETCH_OBJ)){
            $constructeur = new Constructeur(
                $constructeur->id,
                $constructeur->nom
            );

            array_push($constructeurs, $constructeur);
        }

        return $constructeurs;
    }

    public function CreerConstructeurParID($id)
    {
        // On a besoin de la classe Constructeur
        require_once 'Constructeur.php';

        // Notre requete SQL
        $requete = $this->acces->prepare('SELECT * FROM constructeur WHERE id = :id');

        $resultat = $requete->execute([
            'id' => $id
        ]);

        if($resultat){
            $constructeur = $requete->fetch(PDO::FETCH_OBJ);

            $constructeur = new Constructeur(
                $constructeur->id,
                $constructeur->nom
            );

            return $constructeur;
        }
    }

    public function InsererMessage($sujet, $message){
        $requete = $this->acces->prepare("INSERT INTO contact(id, sujet, message) VALUES(DEFAULT, :sujet, :message)");

        $resultat = $requete->execute([
            'sujet' => $sujet,
            'message' => $message
        ]);

        return $resultat;
    }

}