<?php

/**
 * Class Utilisateur
 * Represente un utilisateur de l'application
 * Il peut être administrateur (1), responsable (2) ou simple utilisateur (3)
 * Ces utilisateurs sont crées par un administrateur uniquement
 */

require_once 'ObjetBase.php';

class Utilisateur extends ObjetBase
{
    private $nom;
    private $prenom;
    private $telephone;
    private $mail;
    private $role;

    /**
     * Utilisateur constructor.
     * @param $id
     * @param $nom
     * @param $prenom
     * @param $telephone
     * @param $mail
     * @param $id_role
     */
    public function __construct($id, $nom, $prenom, $telephone, $mail, $role)
    {
        parent::setId($id);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->telephone = $telephone;
        $this->mail = $mail;
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    public function estAdmin(){
        if($this->getRole()->getId() == 1){
            return true;
        }else{
            return false;
        }
    }

}