<?php

require_once 'ObjetBase.php';

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 06/12/2016
 * Time: 10:46
 */
class Constructeur extends ObjetBase
{
    private $nom;

    /**
     * Constructeur constructor.
     * @param $nom
     */
    public function __construct($id, $nom)
    {
        parent::setId($id);
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
}