<?php

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 06/12/2016
 * Time: 10:46
 */
class ObjetBase
{
    private $id;

    /**
     * ObjetBase constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}