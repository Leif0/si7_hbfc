<?php
require_once 'ObjetBase.php';

class Vehicule extends ObjetBase
{
    private $immatriculation;
    private $modele;
    private $mise_en_circulation;
    private $couleur_exterieure;
    private $nombre_portes;
    private $puissance_fiscale;
    private $boite_de_vitesse;
    private $constructeur;

    /**
     * Vehicule constructor.
     * @param $id
     * @param $immatriculation
     * @param $modele
     * @param $mise_en_circulation
     * @param $couleur_exterieure
     * @param $nombre_portes
     * @param $puissance_fiscale
     * @param $boite_de_vitesse
     * @param $id_constructeur
     */
    public function __construct($id, $immatriculation, $modele, $mise_en_circulation, $couleur_exterieure, 
                                $nombre_portes, $puissance_fiscale, $boite_de_vitesse, $constructeur)
    {
        parent::setId($id);
        $this->immatriculation = $immatriculation;
        $this->modele = $modele;
        $this->mise_en_circulation = $mise_en_circulation;
        $this->couleur_exterieure = $couleur_exterieure;
        $this->nombre_portes = $nombre_portes;
        $this->puissance_fiscale = $puissance_fiscale;
        $this->boite_de_vitesse = $boite_de_vitesse;
        $this->constructeur = $constructeur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed $immatriculation
     */
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    /**
     * @return mixed
     */
    public function getMiseEnCirculation()
    {
        return $this->mise_en_circulation;
    }

    /**
     * @param mixed $mise_en_circulation
     */
    public function setMiseEnCirculation($mise_en_circulation)
    {
        $this->mise_en_circulation = $mise_en_circulation;
    }

    /**
     * @return mixed
     */
    public function getCouleurExterieure()
    {
        return $this->couleur_exterieure;
    }

    /**
     * @param mixed $couleur_exterieure
     */
    public function setCouleurExterieure($couleur_exterieure)
    {
        $this->couleur_exterieure = $couleur_exterieure;
    }

    /**
     * @return mixed
     */
    public function getNombrePortes()
    {
        return $this->nombre_portes;
    }

    /**
     * @param mixed $nombre_portes
     */
    public function setNombrePortes($nombre_portes)
    {
        $this->nombre_portes = $nombre_portes;
    }

    /**
     * @return mixed
     */
    public function getPuissanceFiscale()
    {
        return $this->puissance_fiscale;
    }

    /**
     * @param mixed $puissance_fiscale
     */
    public function setPuissanceFiscale($puissance_fiscale)
    {
        $this->puissance_fiscale = $puissance_fiscale;
    }

    /**
     * @return mixed
     */
    public function getBoiteDeVitesse()
    {
        return $this->boite_de_vitesse;
    }

    /**
     * @param mixed $boite_de_vitesse
     */
    public function setBoiteDeVitesse($boite_de_vitesse)
    {
        $this->boite_de_vitesse = $boite_de_vitesse;
    }

    /**
     * @return mixed
     */
    public function getConstructeur()
    {
        return $this->constructeur;
    }

    /**
     * @param mixed $constructeur
     */
    public function setConstructeur($constructeur)
    {
        $this->constructeur = $constructeur;
    }

}