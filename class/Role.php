<?php
require_once 'ObjetBase.php';

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 11/11/2016
 * Time: 17:44
 */
class Role extends ObjetBase
{
    private $libelle;

    /**
     * Role constructor.
     * @param $id
     * @param $libelle
     */
    public function __construct($id, $libelle)
    {
        parent::setId($id);
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

}