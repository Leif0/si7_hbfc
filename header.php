<?php
session_start();

// Un tableau d'erreurs globale pour l'application
$_SESSION['erreurs'] = [];

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>SI7</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Raleway:300" rel="stylesheet">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/lumen.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>