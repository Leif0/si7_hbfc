<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="index.php">SI7</a>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li <?php if(isset($_GET['page']) && $_GET['page'] == 'location') echo 'class="active"' ?>><a href="index.php?page=location">Locations</a></li>
                <li <?php if(isset($_GET['page']) && $_GET['page'] == 'contact') echo 'class="active"' ?>><a href="index.php?page=contact">Contact</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(!isset($_SESSION['id'])): ?>
                    <li><a href="index.php?page=connexion">Connexion</a></li>
                    <?php else: ?>
                        <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1) : ?>
                            <li><a href="index.php?page=administration">Administration</a></li>
                        <?php endif; ?>
                    <li><a href="index.php?page=deconnexion">Deconnexion</a></li>
                <?php endif; ?>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
</nav>