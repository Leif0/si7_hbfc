/**
 * Created by Leif on 12/11/2016.
 */
jQuery(document).ready(function($){
    $('[data-toggle="popover"]').popover('show');


   $('#bouton-ajouter-utilisateur .btn').click(function(e){
       e.preventDefault();
       $('#formulaire-utilisateur').fadeToggle('fast');
   });

    /**
     * Supprimer un utilisateur en AJAX quand on clique sur le bouton supprimer
     */
    $('.supprimer-utilisateur').click(function(e){
        e.preventDefault();

        var idUtilisateur = $(this).attr("data-id");

        if(idUtilisateur){
            if(confirm("Supprimer l'utilisateur ?")){
                $.post({
                    url:"templates/admin/supprimer.php",
                    data: {
                        'id': idUtilisateur
                    },
                    success: function(data){
                        $('#ligne-' + data['idUtilisateur']).remove();
                    }
                });
            }
        }
    });

    /**
     * Modifier un utilisateur quand on clique sur le bouton modifier
     */
    $('.modifier-utilisateur').click(function(e){
        e.preventDefault();

        var idUtilisateur = $(this).attr("data-id");

        if(idUtilisateur){
            var ligne = $('#ligne-'+idUtilisateur);

            var champs = {
                id : ligne.find('.id')[0].innerText,
                nom : ligne.find('.nom')[0].innerText,
                prenom : ligne.find('.prenom')[0].innerText,
                telephone : ligne.find('.telephone')[0].innerText,
                mail : ligne.find('.mail')[0].innerText,
                id_role : $('#ligne-' + idUtilisateur + " .role").attr("data-id")
            };

            var form = $('#formulaire-utilisateur');
            form.fadeIn('fast');

            for (var key in champs) {
                document.getElementById(key).value = champs[key];
            }
        }
    });

    // Clic sur le bouton ajouter vehicule
    $('#bouton-ajouter-vehicule .btn').click(function(e){
        e.preventDefault();
        $('#formulaire-vehicule').fadeToggle('fast');
    });

});