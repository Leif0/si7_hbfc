<?php
function afficher_location_si_connecte(){
    if(isset($_SESSION['id'])){
        require_once 'templates/location.php';
    }else{
        require_once 'templates/connexion.php';
    }
}

function detruire_session(){
    if(isset($_SESSION['id'])){
        $_SESSION['id'] = null;
        session_destroy();
    }
}

function utilisateur_actuel_est_admin(){
    if(isset($_SESSION['role']) && $_SESSION['role'] == 1){
        return true;
    }else{
        return false;
    }
}

function afficher_erreurs(){
    if(isset($_SESSION['erreurs']) && count($_SESSION['erreurs']) > 0){
        echo '<div class="alert alert-danger erreurs" role="alert">';
            echo '<ul>';
                foreach($_SESSION['erreurs'] as $erreur){
                    echo '<li>' . $erreur . '</li>';
                }
            echo '</ul>';
        echo '</div>';

        $_SESSION['erreurs'] = null;
    }
}

function ajouter_erreur($message){
    $erreurs = $_SESSION['erreurs'];
    array_push($erreurs, $message);
    $_SESSION['erreurs'] = $erreurs;
}