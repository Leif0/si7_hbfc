<?php
if($_SERVER['SERVER_NAME'] == 'localhost'){
    $_ENV['env'] = 'local';
}else{
    $_ENV['env'] = 'prod';
}
?>

<?php require_once 'header.php' ?>
<?php require_once 'functions.php' ?>

<?php

// Connexion à la base de donnée avec un objet

require_once 'class/BDD.php';
$BDD = new BDD();

/**
 * On switch sur le paramètre "page" dans l'URL.
 * location : Page avec la recherche de location (date et type de véhicule) si on est connecté
 * connexion : Page de connexion (inscription impossible)
 * deconnexion : Détruit la session et affiche la connexion
 * administration : Espace reservé aux administrateurs (1)
 */

if(isset($_GET['page']))
{

    $page = $_GET['page'];

    switch($page)
    {
        case 'location':
            break;

        case 'recherche':
            $vehicules = $BDD->CreerVehicules();
            require_once 'templates/recherche.php';
            break;

        // TODO : Connexion réelle
        case 'connexion':
            if(isset($_POST['mail']) && isset($_POST['password'])){
                $id_utilisateur = $BDD->ConnexionUtilisateur($_POST['mail'], $_POST['password']);

                if($id_utilisateur != 0){
                    $_SESSION['id'] = $id_utilisateur;
                    $_SESSION['role'] = 1;
                }else{
                    ajouter_erreur("L'email et le mot de passe donnés ne correspondent à aucun utilisateur.");

                    require_once 'templates/connexion.php';
                }
            }
            break;

        case 'deconnexion':
            detruire_session();
            require_once 'templates/connexion.php';
            break;

        case 'administration':
            if(utilisateur_actuel_est_admin()){
                require_once 'templates/admin/index.php';
            }else{
                die('Accès non autorisé');
            }
            break;

        case 'contact':
            require_once 'templates/traitement_contact.php';
            require_once 'templates/contact.php';

            if(isset($_SESSION['id']) && $_SESSION['id'] != null){
                require_once 'templates/traitement_contact.php';
                require_once 'templates/contact.php';
            }else{
                die('Accès non autorisé');
            }
            break;
    }

    if(in_array($page, ['location', 'connexion'])){
        afficher_location_si_connecte();
    }
}
else
{
    afficher_location_si_connecte();
}

?>

<?php require_once 'footer.php' ?>